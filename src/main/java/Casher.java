import java.util.List;
import java.util.concurrent.*;

public class Casher implements Runnable {


    public static void casire() throws InterruptedException {


        CountDownLatch lock = new CountDownLatch(1);

        ScheduledExecutorService executor = Executors.newScheduledThreadPool(1);
        ScheduledFuture<?> future = executor.scheduleAtFixedRate(() -> {
            System.out.println("I am cashier!");

            Bank.setIsAvailable1(false);


            Bank.setIsAvailable2(false);


            List<Acount> acounts = AcountHolder.getAcountList();

            for (Acount ac: acounts) {

                if (ac.isReady()){
                    if (ac.isCreditvzav()){
                        Bank.creditTo(ac);
                        AcountHolder.removeacount(ac);
                        System.out.println("12");
                    } else {
                        Bank.depositeTo(ac);
                        AcountHolder.removeacount(ac);
                        System.out.println("13");
                    }
                }
            }


            Bank.setIsAvailable1(true);

            Bank.setIsAvailable2(true);

            lock.countDown();
        }, 500, 10000, TimeUnit.MILLISECONDS);

        lock.await(2000, TimeUnit.MILLISECONDS);
        future.cancel(true);

    }


    @Override
    public void run() {

        try {
            Thread.sleep(150);
            casire();

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }
}
